#!/usr/bin/env node
const commonmark = require('commonmark')
const fs = require('fs')

const reader = new commonmark.Parser()
const writer = new commonmark.HtmlRenderer()

function md2html(input) {
    let html = writer.render(reader.parse(input))
    
    // Add class="full-bleed" to images and remove <p> tags encapsulating it
    let re = /<p><img src(.*)<\/p>/gi
    let replaced = html.replace(re, '<img class="full-bleed" src$1')
    return replaced
}

fs.readFile(process.argv[2], 'utf-8', function (err, data) {
    if (err) {
        console.log(err) // might not want this here..
    }
    else {
        console.log(md2html(data))
    }
})
