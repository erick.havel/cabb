#!/bin/bash

CABB_VERSION="0.1.0"
VERSION_HASH=$(echo "$CABB_VERSION"|shasum|cut -d" " -f1|cut -c1-6)

# get settings
if [[ ! -f settings.sh ]]; then
    echo
    echo "settings.sh not found, quitting"
    exit 1
fi

RENDER_ALL="false"
if [[ "$1" == "--render-all" ]]; then
    RENDER_ALL="true"
fi

source settings.sh

# quick sloppy create draft preview function
for post in $(find posts -type f|grep -E "\.draft$"); do
    echo "Creating draft of $post"
    echo "---"

    DRAFT_POST_SLUG=$(echo $post|cut -d"/" -f2)
    DRAFT_HTML_FILENAME="$DRAFT_POST_SLUG.html"

    DRAFT_POST_TITLE=$(head -n1 $post|sed -E "s/\# ?//")
    ESCAPED_DRAFT_POST_TITLE=$(printf '%s\n' "$DRAFT_POST_TITLE" | sed -e 's/[\/&]/\\&/g')
    ESCAPED_DRAFT_SITE_URL=$(printf '%s\n' "$SITE_URL" | sed -e 's/[\/&]/\\&/g')

    cp -R template template-tmp

    sed -i.bak "s/---POST_TITLE---/$ESCAPED_DRAFT_POST_TITLE/" template-tmp/header.html
    sed -i.bak "s/---SITE_URL---/$ESCAPED_DRAFT_SITE_URL/" template-tmp/header.html

    # add version hash to CSS
    sed -i.bak "s/prism\.css/prism-$VERSION_HASH.css/" template-tmp/header.html

    cat template-tmp/header.html >> template-tmp/$DRAFT_HTML_FILENAME

    echo "</head>" >> template-tmp/$DRAFT_HTML_FILENAME


    sed -i.bak "s/---POST_TITLE---/$ESCAPED_DRAFT_POST_TITLE/" template-tmp/post-top.html
    cat template-tmp/post-top.html >> template-tmp/$DRAFT_HTML_FILENAME

    # Generate HTML from MD and append to output file
    node utilities/md2html.js $post|tail -n +2 >> template-tmp/$DRAFT_HTML_FILENAME

    # add version hash to JS
    sed -i.bak "s/prism\.js/prism-$VERSION_HASH.js/" template-tmp/header.html

    cat template-tmp/footer.html >> template-tmp/$DRAFT_HTML_FILENAME

    cp template-tmp/$DRAFT_HTML_FILENAME public/$DRAFT_HTML_FILENAME

    echo "Draft published in public/$DRAFT_HTML_FILENAME"
    echo "You should be able to reach it at $SITE_URL/$DRAFT_HTML_FILENAME"
done

for post in $(find posts -type f|grep -E "\.md$"); do
    echo
    echo "Processing $post"
    echo "---"

    # Generate post slug and output filename
    POST_SLUG=$(echo $post|cut -d"." -f1|cut -d"/" -f2)
    HTML_FILENAME="$POST_SLUG.html"

    if [[ "$RENDER_ALL" == "false" ]]; then
        # Check hashes and determine whether to render or not
        SHOULD_RENDER="false"
        NEW_POST="false"
        POST_HASH=$(shasum $post|cut -d" " -f1)
        if [[ ! -f "posts/${POST_SLUG}.hash" ]]; then # if hash file doesn't exist
            echo "New post, saving hash"
            echo "$POST_HASH" > posts/${POST_SLUG}.hash
            SHOULD_RENDER="true"
            NEW_POST="true"
            # Delete any existing draft
            rm -f public/${POST_SLUG}.md.draft.html
        fi

        if [[ "$NEW_POST" == "false" ]]; then
            if [[ "$(cat posts/${POST_SLUG}.hash)" == "$POST_HASH" ]]; then # if old post and hashes match
                echo "Hashes match, no need to render"
                continue
            else
                echo "Hashes don't match, need to render"
                echo "$POST_HASH" > posts/${POST_SLUG}.hash
                SHOULD_RENDER="true"
            fi
        fi
    fi

    # Remove workdir if it happens to exist
    rm -rf template-tmp

    # Copy the template to working dir
    cp -R template template-tmp

    # TODO: This certainly needs improvement due to BSD sed gayness on Mac...
    # maybe I'll just write a bunch of Node utility scripts instead...
    POST_TITLE=$(head -n1 $post|sed -E "s/\# ?//")

    if [[ "$HTML_FILENAME" == "index.html" ]]; then
        echo
        echo "!! Error: You cannot name your post index.md as it would clash with the frontpage index"
        echo "!! Skipping"
        continue
    fi

    # Get file modification timestamp (depending on OS, no idea what Windoze would show)
    if [[ "$(uname)" == "Linux" ]]; then
        POST_MODIFIED_UTC_TIMESTAMP=$(date +%s -r $post)
    elif [[ "$(uname)" == "Darwin" ]]; then
        POST_MODIFIED_UTC_TIMESTAMP=$(stat -f%c $post)
    fi

    # If this is a brand new post, add timestamp to file to be used as "published" date
    if [[ ! -f "posts/$POST_SLUG.timestamp" ]]; then
        echo $POST_MODIFIED_UTC_TIMESTAMP > posts/$POST_SLUG.timestamp
    fi

    # Set published timestamp from file, due to the above regardless of whether it was just created
    POST_PUBLISHED_UTC_TIMESTAMP=$(cat posts/$POST_SLUG.timestamp)

    # Check if post has been modified
    if [[ $POST_MODIFIED_UTC_TIMESTAMP -eq $POST_PUBLISHED_UTC_TIMESTAMP ]]; then
        # Really crude and ugly way to not show Edited date in post if it's the same as published
        # This method needs to be changed/improved since it pretty much breaks with any customization to post-top.html
        echo "Post not modified, hiding modified date"
        awk 'NR!~/^(7)$/' template-tmp/post-top.html > template-tmp/tmp && mv template-tmp/tmp template-tmp/post-top.html
    fi

    # Create readable date strings for UTC
    POST_PUBLISHED_UTC=$(node -e "\
        let d = new Date(${POST_PUBLISHED_UTC_TIMESTAMP}000); \
        let year = d.getFullYear(); \
        let month = d.toLocaleString('default', { month: 'long' }); \
        let day = d.getDate();
        let hours = ('0' + (d.getUTCHours())).slice(-2); \
        let minutes = ('0' + (d.getUTCMinutes())).slice(-2); \
        console.log(month + ' ' + day + ', ' + year + ' @ ' + hours + ':' + minutes + ' UTC');")
    POST_MODIFIED_UTC=$(node -e "\
        let d = new Date(${POST_MODIFIED_UTC_TIMESTAMP}000); \
        let year = d.getFullYear(); \
        let month = d.toLocaleString('default', { month: 'long' }); \
        let day = d.getDate(); \
        let hours = ('0' + (d.getUTCHours())).slice(-2); \
        let minutes = ('0' + (d.getUTCMinutes())).slice(-2); \
        console.log(month + ' ' + day + ', ' + year + ' @ ' + hours + ':' + minutes + ' UTC');")
    # These are used in the JSON+LD markup
    POST_PUBLISHED_DATE_ISO8601=$(node -e "\
        let d = new Date(${POST_PUBLISHED_UTC_TIMESTAMP}000); \
        let year = d.getFullYear(); \
        let month = ('0' + (d.getMonth()+1)).slice(-2); \
        let day = ('0' + d.getDate()).slice(-2); \
        let hours = ('0' + (d.getUTCHours())).slice(-2); \
        let minutes = ('0' + (d.getUTCMinutes())).slice(-2); \
        let seconds = ('0' + (d.getUTCSeconds())).slice(-2); console.log(year + '-' + month + '-' + day + 'T' + hours + ':' + minutes + ':' + seconds + 'Z');")
    POST_MODIFIED_DATE_ISO8601=$(node -e "\
        let d = new Date(${POST_MODIFIED_UTC_TIMESTAMP}000); \
        let year = d.getFullYear(); \
        let month = ('0' + (d.getMonth()+1)).slice(-2); \
        let day = ('0' + d.getDate()).slice(-2); \
        let hours = ('0' + (d.getUTCHours())).slice(-2); \
        let minutes = ('0' + (d.getUTCMinutes())).slice(-2); \
        let seconds = ('0' + (d.getUTCSeconds())).slice(-2); console.log(year + '-' + month + '-' + day + 'T' + hours + ':' + minutes + ':' + seconds + 'Z');")

    # Output for debugging
    echo "Title: $POST_TITLE"
    echo "Published: $POST_PUBLISHED_UTC"
    echo "Modified: $POST_MODIFIED_UTC"
    echo "$POST_PUBLISHED_DATE_ISO8601"
    echo "$POST_MODIFIED_DATE_ISO8601"
    echo "Output: public/$HTML_FILENAME"
    
    # Replace title within <title> tags in header
    ESCAPED_POST_TITLE=$(printf '%s\n' "$POST_TITLE" | sed -e 's/[\/&]/\\&/g') # thank you Pianosaurus! https://stackoverflow.com/a/2705678/14237590
    sed -i.bak "s/---POST_TITLE---/$ESCAPED_POST_TITLE/" template-tmp/header.html
    
    META_DESCRIPTION=$(cat $post|head -n 2|tail -n 1)
    ESCAPED_META_DESCRIPTION=$(printf '%s\n' "$META_DESCRIPTION" | sed -e 's/[\/&]/\\&/g')
    META_DESCRIPTION_LENGTH=$(echo $META_DESCRIPTION|wc -c|sed "s/ *//")
    ESCAPED_SITE_URL=$(printf '%s\n' "$SITE_URL" | sed -e 's/[\/&]/\\&/g')
    if [[ $META_DESCRIPTION_LENGTH -gt 1 ]]; then
        echo "Meta description: $META_DESCRIPTION"
        if [[ $META_DESCRIPTION_LENGTH -gt 160 ]]; then
            echo "!! Warning: You should strive to keep your meta description below 160 characters (this one is $META_DESCRIPTION_LENGTH)"
        fi
        # Add meta description to header        
        sed -i.bak "s/---META_DESCRIPTION---/$ESCAPED_META_DESCRIPTION/" template-tmp/header.html
    else
        echo "!! Warning: Meta description missing"
        # Add empty meta description to header        
        sed -i.bak "s/---META_DESCRIPTION---//" template-tmp/header.html
    fi

    sed -i.bak "s/---SITE_URL---/$ESCAPED_SITE_URL/" template-tmp/header.html

    # add version hash to CSS
    sed -i.bak "s/prism\.css/prism-$VERSION_HASH.css/" template-tmp/header.html

    # Create JSON-LD
    ESCAPED_SITE_AUTHOR=$(printf '%s\n' "$SITE_AUTHOR" | sed -e 's/[\/&]/\\&/g')
    sed -i.bak "s/---SITE_AUTHOR---/$ESCAPED_SITE_AUTHOR/" template-tmp/json-ld-post
    ESCAPED_SITE_NAME=$(printf '%s\n' "$SITE_NAME" | sed -e 's/[\/&]/\\&/g')
    sed -i.bak "s/---SITE_NAME---/$ESCAPED_SITE_NAME/" template-tmp/json-ld-post
    sed -i.bak "s/---POST_TITLE---/$ESCAPED_POST_TITLE/" template-tmp/json-ld-post
    sed -i.bak "s/---POST_PUBLISHED_DATE_ISO8601---/$POST_PUBLISHED_DATE_ISO8601/" template-tmp/json-ld-post
    sed -i.bak "s/---POST_MODIFIED_DATE_ISO8601---/$POST_MODIFIED_DATE_ISO8601/" template-tmp/json-ld-post
    # Add JSON-LD to head
    cat template-tmp/json-ld-post >> template-tmp/header.html

    echo "</head>" >> template-tmp/header.html

    # Remove the stupid backup file (needed for GNU/BSD compatibility)
    rm template-tmp/header.html.bak

    # Append header.html to output file
    cat template-tmp/header.html >> template-tmp/$HTML_FILENAME

    # Replace H1 title in post-top.html template
    sed -i.bak "s/---POST_TITLE---/$ESCAPED_POST_TITLE/" template-tmp/post-top.html

    # Replace dates in post-top.html template
    sed -i.bak "s/---POST_PUBLISHED_UTC---/$POST_PUBLISHED_UTC/" template-tmp/post-top.html    
    sed -i.bak "s/---POST_MODIFIED_UTC---/$POST_MODIFIED_UTC/" template-tmp/post-top.html

    # Replace site url for Home link
    sed -i.bak "s/---SITE_URL---/$ESCAPED_SITE_URL/" template-tmp/post-top.html
    
    # Append post-top.html to output file
    cat template-tmp/post-top.html >> template-tmp/$HTML_FILENAME

    cp $post template-tmp/post.md
    if [[ $META_DESCRIPTION_LENGTH -gt 1 ]]; then
        # remove the meta description from the markdown post before rendering it
        awk 'NR!~/^(2)$/' $post > template-tmp/tmp && mv template-tmp/tmp template-tmp/post.md
    fi

    # Generate HTML from MD and append to output file
    node utilities/md2html.js template-tmp/post.md|tail -n +2 >> template-tmp/$HTML_FILENAME
    
    # Replace SITE_URL and append footer.html to output file
    sed -i.bak "s/---SITE_URL---/$ESCAPED_SITE_URL/" template-tmp/footer.html

    # add version hash to JS
    sed -i.bak "s/prism\.js/prism-$VERSION_HASH.js/" template-tmp/header.html

    cat template-tmp/footer.html >> template-tmp/$HTML_FILENAME
    
    # Publish output file
    mv template-tmp/$HTML_FILENAME public/
    
    # Remove workdir
    rm -rf template-tmp
done

# Render frontpage.... i.e. public/index.html

cp -R template template-tmp

echo
echo "Rendering index.html"

# Add site title
ESCAPED_SITE_NAME=$(printf '%s\n' "$SITE_NAME" | sed -e 's/[\/&]/\\&/g')
sed -i.bak "s/---POST_TITLE---/$ESCAPED_SITE_NAME/" template-tmp/header.html

# Add meta description
ESCAPED_SITE_DESCRIPTION=$(printf '%s\n' "$SITE_DESCRIPTION" | sed -e 's/[\/&]/\\&/g')
sed -i.bak "s/---META_DESCRIPTION---/$ESCAPED_SITE_DESCRIPTION/" template-tmp/header.html

# Create JSON-LD
sed -i.bak "s/---SITE_NAME---/$ESCAPED_SITE_NAME/" template-tmp/json-ld-index
ESCAPED_SITE_URL=$(printf '%s\n' "$SITE_URL" | sed -e 's/[\/&]/\\&/g')
sed -i.bak "s/---SITE_URL---/$ESCAPED_SITE_URL/" template-tmp/json-ld-index

# Add JSON-LD to head
cat template-tmp/json-ld-index >> template-tmp/header.html

echo "</head>" >> template-tmp/header.html

rm template-tmp/header.html.bak

# add header to index.html
cat template-tmp/header.html > template-tmp/index.html

# Replace H1 title in post-top.html template
sed -i.bak "s/---POST_TITLE---/$ESCAPED_SITE_NAME/" template-tmp/post-top.html
head -n4 template-tmp/post-top.html > template-tmp/tmp && mv template-tmp/tmp template-tmp/post-top.html

# Replace site url for Home link
ESCAPED_SITE_URL=$(printf '%s\n' "$SITE_URL" | sed -e 's/[\/&]/\\&/g')
sed -i.bak "s/---SITE_URL---/$ESCAPED_SITE_URL/" template-tmp/post-top.html

echo "<hr />" >> template-tmp/post-top.html

cat template-tmp/post-top.html >> template-tmp/index.html

# Create ordered list
rm -f template-tmp/posts-unordered.list template-tmp/posts-ordered.list
for post in $(find posts -type f|grep "\.md$"); do
    TS=$(echo $post|sed "s/\.md$//") # remove .md from the end
    TS=$(cat ${TS}.timestamp) # get published timestamp
    echo "$TS $post" >> template-tmp/posts-unordered.list # add to list
done
cat template-tmp/posts-unordered.list|sort -r > template-tmp/posts-ordered.list # sort list in reverse order (newest/largest timestamps on top)

echo "<div class=\"frontpage\">" >> template-tmp/index.html

if [[ "$PAGINATION" == "false" ]]; then
    # Neverending stream of pages...
    while read -r post; do
        post=$(echo $post|cut -d" " -f2) # remove timestamp column
        POST_TITLE=$(head -n1 $post|sed -E "s/\# ?//")
        POST_SLUG=$(echo $post|cut -d"." -f1|cut -d"/" -f2)
        POST_PUBLISHED_UTC_TIMESTAMP=$(cat posts/$POST_SLUG.timestamp)
        # adding some lazy duplication here as this is needed now after the hashing skips running through posts
        POST_PUBLISHED_DATE_UTC=$(node -e "\
            let d = new Date(${POST_PUBLISHED_UTC_TIMESTAMP}000); \
            let year = d.getFullYear(); \
            let month = d.toLocaleString('default', { month: 'long' }); \
            let day = d.getDate();
            console.log(month + ' ' + day + ', ' + year);")
        HTML_FILENAME="$POST_SLUG.html"
        META_DESCRIPTION=$(cat $post|head -n 2|tail -n 1)
        echo "<div class=\"post-wrapper\">" >> template-tmp/index.html
        echo "<p class=\"heading\"><a href=\"$HTML_FILENAME\"  class=\"heading-link\">$POST_TITLE</a></p>" >> template-tmp/index.html
        echo "<p class=\"meta-description\">$META_DESCRIPTION</p>" >> template-tmp/index.html
        echo "<p class=\"bottom-row\"><span>$POST_PUBLISHED_DATE_UTC</span> <span class=\"read-more\"><a href=\"$HTML_FILENAME\">Read more...</a></span></p>" >> template-tmp/index.html
        echo "</div>" >> template-tmp/index.html
    done < template-tmp/posts-ordered.list
# else
    # TODO: Here we will need to create the pagination
fi

echo "</div>" >> template-tmp/index.html

# Append footer.html to index.html
cat template-tmp/footer.html >> template-tmp/index.html

# Publish index.html
mv template-tmp/index.html public
rm -rf template-tmp

# Copy current version JS/CSS into place (keeping the old versions deliberately since there might be cached references to them in some cases)
mkdir -p public/{css,js}
cp assets/css/prism.css public/css/prism-$VERSION_HASH.css
cp assets/js/prism.js public/js/prism-$VERSION_HASH.js
