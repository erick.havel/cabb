#!/bin/bash

if [[ $# -lt 1 ]]; then
    echo "Usage:"
    echo "./create.sh \"<title>\""
    exit 1
fi

source settings.sh

TITLE="$1"

SLUG=$(echo $TITLE| tr '[:upper:]' '[:lower:]')
SLUG=$(echo $SLUG|sed "s/[#,\.'\!&\?]//g"|sed "s/[ \/]/-/g"|sed "s/---/-/g"|sed "s/--/-/g")

if [[ "$SLUG" == "index" ]]; then
    echo "You can't name a post index"
    exit 1
else
    if [[ -f "posts/$SLUG.md" ]]; then
        echo "posts/$SLUG.md already exits :'("
        exit 1
    else
        read -p "Create posts/$SLUG.md? (Y/n) " response
        if [[ "$response" == "n" ]]; then
            echo "Quitting"
            exit 1
        fi
    fi
fi

echo "Creating post $SLUG.md and opening in editor ($POST_EDITOR)"
echo "# $TITLE" > posts/$SLUG.md

if [[ "$(uname)" == "Darwin" ]]; then
    # Here's a bit of a hack to get /Applications/Sublime Text.app/Contents/SharedSupport/bin/subl to work on MacOS...
    ln -s "$POST_EDITOR" utilities/editor
    utilities/editor posts/$SLUG.md
    rm utilities/editor
else
    $POST_EDITOR posts/$SLUG.md
fi
