# CABB - A static site generator

This is intended to be a super simple static site generator. Rudimentary templating, highly opinionated and tailored towards my specific use case and needs. I could've used a fancy templating engine but preferred to create as much of this whole thing as possible by myself. The only third-party library used here is [Commonmark.js](https://github.com/commonmark/commonmark.js) and it only adds around 800 kB to the project. There would've been a [Markdown to HTML generator for Bash](https://github.com/chadbraunduin/markdown.bash) and would've gone with a Bash only solution, but unfortunately `markdown.bash` doesn't support [fenced codeblocks](https://spec.commonmark.org/0.28/#fenced-code-blocks) which was a requirement for me, so I ended up introducing Node.

## Requirements/dependencies

* Node.js
* Yarn or NPM
* Bash on MacOS/Linux, not tested on Windows

## Installation

Clone repo and run:
```
yarn install

cp sample.settings.sh settings.sh
```

Edit `settings.sh` to taste.

## Usage

1. Create markdown posts in `posts/*`
2. Manually add any media (images/video) to `public/media/*`
3. Render the pages with `./render.sh` (files are rendered in `public/*`)

The idea is to run this locally (or wherever you want for that matter), with a webserver serving the `public/` directory. For deployment you'd render locally, push to git, then pull on the server. This can be automated however you like. Could for example add a `deploy.sh` script something like:

```
#!/bin/bash

./render.sh

git add .
git commit -m "Whatever"
git push origin master
ssh <yourserver> "cd /path/to/repo && bash render.sh"
```

Or I might actually, since I'm using [Gitolite](https://github.com/sitaramc/gitolite) just serve `/home/git/repositories/cabb/public` and changes would be automatically deployed on push.

## Markdown source structure

The basic format looks like this:
```
# Post title
Description of the post

Actual post content.
```

This will add `<title>Post title</title>` and `<meta name="description" content="Description of the post">` to the `<head>`.

The second line (description) is also used on the frontpage listing to describe the post. It's optional and can be left out, even if it's recommended:

```
# Post title

Actual post content.
```

Wrong:
```
# Post title
Actual post content.
```
The second line would used for the description - if you don't want to use it, just leave it empty.

## Post creation tool

With the `create.sh` script you can automatically create a post from the command line and fire it up in your favorite editor (which you define in `settings.sh`). The standard is:

```
POST_EDITOR="vim"
```

But if you for example want to use Sublime on MacOS, here's what you'd do:
```
POST_EDITOR="/Applications/Sublime Text.app/Contents/SharedSupport/bin/subl"
```

When the editor is setup, you can simpl run:
```
$ ./create.sh "Here's my new & awesome post"
Create posts/heres-my-new-awesome-post.md? (Y/n)
Creating post heres-my-new-awesome-post.md and opening in editor (/Applications/Sublime Text.app/Contents/SharedSupport/bin/subl)
```

And your favorite editor opens up with the title already `# Here's my new & awesome post` in place.

## TODO

* pagination
* clean up `sed -i` (don't use backup files, instead use native sed commands depending on OS)
* possibly add dates (`YYYY/MM`) to URL's
* add flag `--no-modify` to not update the modified date when rendering
* create an escape function, too much repetition going on
* add support for non-full-bleed images
* improve post preview function (add it to create.sh, add dates and meta description (for more visual appeal))
* add `--ninja-edit` flag which disables updating the modified timestamps (requires some trickery like to `touch` the `.md` file with the timestamp from the `.timestamp` file, otherwise it'd just get updated on the next render without the flag)

~~* create simple post preview function~~
~~* currently pages are sorted quite randomly, using `find posts -type f|grep "\.md$"`, should be changed to date published, descending (newest on top)~~
~~* add hash of file to `posts/<slug>.hash` and check it in order to not rebuild every post on every render~~
~~* nicer looking dates (also on the index page)~~

## Bugs

None, of course.
